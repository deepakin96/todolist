How to run the build?
Goto project directory /todolist/ 
Run -> java -jar todolist-0.0.1-SNAPSHOT.war
Visit: localhost:8080


OR

Goto project directory /todolist/ 
Run -> mvn clean spring-boot:run
Visit: localhost:8080


Features implement
Users sign up
User Login (default username: test and password: pwd123)
User can add Todo list with description and Due date
User can update the Todo list
User can delete the Todo list
User can update status of the Todo list.
User can view the Todo list with description, due date, status and modified date.

Architecture Used:
MVC Framework Architecture

Why Spring framework were used?
Spring is having some of the advantages as follows
1.	Separate roles
2.	Light-weight
3.	Powerful Configuration
4.	Rapid development
5.	Reusable business code
6.	Easy to test
7.	Flexible Mapping

Design Patterns used:
1.	MVC design Pattern.
2.	Singleton Pattern
3.	Proxy Pattern
4.	Factory Method Pattern
5.	Template Method Pattern


Code Explanation
The code is designed in model-controller-view (MVC) model. It is designed to two controller having number of end points in each controller class. The sign up is designed with JWT authentication. Web security have been configured with JWT authentication.

JPARepository interface is used to provide the JPA feature around the domain class.  

H2 is configured to store in-memory data. Exception are configured and handled in the code.

The front end is developed and deployed as prod in the project. Angular is used in the front end as it is ease to integrate with the end point API. However front end has basic functional code which was not focused much.
