package ie.deloitte.domain;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}