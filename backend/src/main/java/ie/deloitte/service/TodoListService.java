package ie.deloitte.service;

import java.util.List;
import java.util.Optional;

import ie.deloitte.config.auth.UserPrincipal;
import ie.deloitte.domain.Todo;

public interface TodoListService {
	
	List<Todo> todoListByUserId(Long userId);

	Optional<Todo> findById(Long todoId);

	Todo save(UserPrincipal currentUser, Todo todo);

	void delete(Todo todo);

}
