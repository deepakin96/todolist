package ie.deloitte.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.deloitte.config.auth.UserPrincipal;
import ie.deloitte.domain.Todo;
import ie.deloitte.domain.User;
import ie.deloitte.repository.TodoRepository;
import ie.deloitte.repository.UserRepository;
import ie.deloitte.service.TodoListService;

@Service
public class TodoListServiceImpl implements TodoListService {

	@Autowired
	private TodoRepository todoRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public List<Todo> todoListByUserId(Long userId) {
		return todoRepository.findAllByUserId(userId);
	}

	@Override
	public Optional<Todo> findById(Long todoId) {
		return todoRepository.findById(todoId);
	}

	@Override
	public Todo save(UserPrincipal currentUser, Todo todo) {
		Optional<User> user = userRepository.findById(currentUser.getId());
		todo.setUser(user.get());
		
		return todoRepository.save(todo);
	}

	@Override
	public void delete(Todo todo) {
		todoRepository.delete(todo);
	}

}
