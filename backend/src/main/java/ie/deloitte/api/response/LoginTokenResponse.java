package ie.deloitte.api.response;

import java.io.Serializable;

public class LoginTokenResponse implements Serializable {

  private static final long serialVersionUID = 8317676219297719109L;

  private final String token;

    public LoginTokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}